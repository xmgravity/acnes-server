var fs = require('fs');

module.exports = function (io) {
  io.on('connection', function(socket) {
    console.log('a user connected');

    socket.on('change_state', function(state) {
      console.log('changing state to ' + state);

      io.emit('changeState', state);
      
      // var file = 'stat.txt';
      // fs.readFile(file, 'utf8', function (err, data) {
      //   if (err) {
      //     return console.log(err);
      //   }

      //   var oldNumber = data;

      //   fs.writeFile(file, +oldNumber + 1, 'utf8', function (err) {
      //      if (err) return console.log(err);
      //   });
      // });

    });
  });
}
