var express = require('express');
var router = express.Router();
var io = require('socket.io-client');
var fs = require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/add_view', function(req, res, next) {
  var file = 'stat.txt';
  fs.readFile(file, 'utf8', function (err, data) {
    if (err) {
      return console.log(err);
    }

    var oldNumber = data;

    fs.writeFile(file, +oldNumber + 1, 'utf8', function (err) {
       if (err) return console.log(err);
    });
  });
});

router.get('/controller', function(req, res, next) {
  // var socket = io('http://localhost:3000');
  // socket.emit('change_state', '2');

  res.render('controller')
});

module.exports = router;
